# $Id: Makefile 5 2006-01-12 21:41:36Z matled $

CC=avr-gcc
LD=avr-gcc
RM=rm -f --
MCU=attiny2313
ISP=usbasp

-include Makefile.local

OBJECTS+= firmware.o
HEADERS+=$(shell echo *.h)
TARGET=firmware

LDFLAGS+=
CFLAGS+=-std=c99 -pedantic-errors -Wall -W -mmcu=$(MCU)

ifneq ($(DEBUG),) # {{{
	CFLAGS+=-ggdb3
	CFLAGS+=-Wchar-subscripts -Wmissing-prototypes
	CFLAGS+=-Wmissing-declarations -Wredundant-decls
	CFLAGS+=-Wstrict-prototypes -Wshadow -Wbad-function-cast
	CFLAGS+=-Winline -Wpointer-arith -Wsign-compare
	CFLAGS+=-Wunreachable-code -Wdisabled-optimization
	CFLAGS+=-Wcast-align -Wwrite-strings -Wnested-externs -Wundef
	CFLAGS+=-DDEBUG
	LDFLAGS+=
else
	CFLAGS+=-O2
	LDFLAGS+=
endif
# }}}
ifneq ($(PROFILING),) # {{{
	CFLAGS+=-pg -DPROFILING
	LDFLAGS+=-pg
endif
# }}}

all: $(TARGET)

clean:
	$(RM) $(TARGET) $(OBJECTS) $(TARGET).hex

.PHONY: clean all

%.o: $(HEADERS)

$(TARGET): $(OBJECTS)
	$(LD) -o $@ $^ $(LDFLAGS)

$(TARGET).hex: $(TARGET)
	avr-objcopy -R .eeprom -O ihex $^ $@
	avr-size $@

load: $(TARGET).hex
	avrdude -p $(MCU) -U flash:w:$^.hex -c $(ISP)
