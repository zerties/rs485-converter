
 /* Copyright(C) 2008 Christian Dietrich <stettberger@dokucode.de>

 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 3 of the License, or
 (at your option) any later version.

 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

 You should have received a copy of the GNU General Public License
 along with this program; if not, write to the Free Software Foundation,
 Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
*/

#define F_CPU 8000000L
#include <util/delay.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdint.h>
#include <avr/eeprom.h>

#define RTS_INT SIG_INTERRUPT0
#define TX_INT SIG_INTERRUPT1

#define DE_PORT PORTD
#define DE_DDR DDRD
#define DE_IN PIND
#define DE_PIN 4

#define RECV_IN PINA
#define RECV_PIN 0

#define TX_PORT PORTB
#define TX_DDR DDRB
#define TX_LED 1

#define RX_PORT PORTD
#define RX_DDR DDRD
#define RX_LED 6

static inline void
timer_init(void)
{
  /* 256 Prescaler */
  TCCR0B = _BV(CS02);
  OCR0A = 33;

  /* Int. bei Overflow und CompareMatch einschalten */
  TIMSK |= _BV(OCIE0A); 
}

static inline void
interrupt_init(void) 
{
  /* Interrupt on: 
   * - rising edge of RTS
   * - any logical change of TX 
   */
  MCUCR |= _BV(ISC01) | _BV(ISC00) | _BV(ISC10);
  GIMSK |= _BV(INT0) | _BV(INT1);
}


SIGNAL(RTS_INT) {
  DE_PORT |= _BV(DE_PIN);
  TCNT0 = 0;
}

SIGNAL(TX_INT) {
  TCNT0 = 0;
}

SIGNAL(SIG_OUTPUT_COMPARE0A) {
  DE_PORT &= ~_BV(DE_PIN);
}

int
main(void)
{
  DE_DDR |= _BV(DE_PIN);
  TX_DDR |= _BV(TX_LED);
  RX_DDR |= _BV(RX_LED); 
  timer_init();
  interrupt_init();
  sei();
  for(;;) {
    if (DE_IN & _BV(DE_PIN))
      TX_PORT |= _BV(TX_LED);
    else
      TX_PORT &= ~_BV(TX_LED);
    if (!(RECV_IN & _BV(RECV_PIN)))
      RX_PORT |= _BV(RX_LED);
    else
      RX_PORT &= ~_BV(RX_LED);
  }
}
  
